FROM python:3.7

COPY config/requirement.txt .
RUN pip install -r requirement.txt

COPY src/lostinart .
WORKDIR /lostinart

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "5000"]